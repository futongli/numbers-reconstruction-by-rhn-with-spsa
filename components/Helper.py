import numpy as np
import matplotlib.pyplot as plt
import pickle
from random import randint


"""
The following two function are used for Test Data Generation.
"""


def display_image(vec, numrow, numcol, name, node_num):
    result = [[0 for x in range(numcol)] for y in range(numrow)]
    ptr = 0

    for x in range(numrow):
        for y in range(numcol):
            result[x][y]= vec[ptr]
            ptr += 1
    plt.subplot()
    plt.imshow(result, interpolation="nearest")
    plt.savefig('results\\' + str(node_num) + '_hidden_nodes\\' + str(name) + '.png')
    plt.show()


def apply_distortion(testdata, numdistortion):
    result = np.copy(testdata)
    veclen = len(result[0])
    for i in range(len(testdata)):
        for j in range(numdistortion):
            p = randint(0, veclen-1)
            if result[i][p] == 0:
                result[i][p] = 1
            else:
                result[i][p] = 0
    return result


"""
The following three functions are used for RHN training and testing.
"""


def initialize_parameters(hidden_num, visible_num, feekback_num):
    # W := (the # of nodes of layer l) * (the # of nodes of layer l+1)
    # b := the # of nodes in layer l
    np.random.seed()
    W = []
    b = []
    w_h = np.random.randn(hidden_num, visible_num) * 0.01
    w_v = w_h.T
    b_h = np.zeros((hidden_num, 1))
    b_v = np.zeros((visible_num, 1))
    for l in range(feekback_num + 1):
        W.append(w_h)
        W.append(w_v)
        b.append(b_h)
        b.append(b_v)
    return W, b


def sigmoid(Z, annealing):
    A = 1/(1+np.exp(-Z / annealing))
    return A


def propagation(A, W, b, annealing):
    Z = np.dot(W, A) + b
    A = sigmoid(Z, annealing)
    return A


def propagation_matching(X, weight, bias, annealing=1):
    A = X
    L = len(weight)  # layers in the neural network
    for l in range(L):
        A = propagation(A, weight[l], bias[l], annealing)
    return A