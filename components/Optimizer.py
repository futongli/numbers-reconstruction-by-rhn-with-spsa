import numpy as np
from components.Helper import propagation_matching


class SPSA:
    """
    An optimizer class that implements Simultaneous Perturbation Stochastic Approximation (SPSA)
    """
    def __init__(self, a, c, A, alpha, gamma):
        # Initialize gain parameters and decay factors
        self.a = a
        self.c = c
        self.A = A
        self.alpha = alpha
        self.gamma = gamma

        self.min_vals = -4.0
        self.max_vals = 4.0

        # counters
        self.t = 0

    def step(self, current_estimate_W, current_estimate_b, feedback_num, data):
        """
        :param current_estimate_w: This is the current estimate of the parameter vector for weights
        :param current_estimate_b: This is the current estimate of the parameter vector for biases
        :param feedback_num: the times of bouncing between the visible layer and the hidden layer for RHN
        :param data: memory data
        :return: returns the updated estimate of the vectors
        """
        L = len(current_estimate_W)  # layers in the neural network
        m = data.shape[1]  # the number of training data
        # get the current values for gain sequences
        a_t = self.a / (self.t + 1 + self.A) ** self.alpha
        c_t = self.c / (self.t + 1) ** self.gamma

        # Measure the loss function at perturbations
        # Get the random perturbation vector from bernoulli distribution which has to be symmetric around zero
        # But normal distribution does not work which makes the perturbations close to zero
        # Also, uniform distribution should not be used since they are not around zero
        deltas_W = []
        deltas_b = []  # Store corresponding delta for every pair of weights and biases
        delta_w_h = np.random.randint(0, 2, current_estimate_W[0].shape) * 2 - 1  # delta for weights
        delta_w_v = delta_w_h.T
        for l in range(feedback_num + 1):
            deltas_W.append(delta_w_h)
            deltas_W.append(delta_w_v)
            deltas_b.append(np.random.randint(0, 2, current_estimate_b[0].shape) * 2 - 1)  # delta for visible biases
            deltas_b.append(np.random.randint(0, 2, current_estimate_b[1].shape) * 2 - 1)  # delta for hidden biases

        # Adjust current weights according to the perturbation vectors
        current_estimate_W_plus = []
        current_estimate_W_minus = []
        current_estimate_b_plus = []
        current_estimate_b_minus = []
        for i in range(L):
            current_estimate_W_plus.append(current_estimate_W[i] + deltas_W[i] * c_t)
            current_estimate_W_minus.append(current_estimate_W[i] - deltas_W[i] * c_t)
            current_estimate_b_plus.append(current_estimate_b[i] + deltas_b[i] * c_t)
            current_estimate_b_minus.append(current_estimate_b[i] - deltas_b[i] * c_t)

        # Compute J(w+) and J(w-)
        J_plus = np.sum((data - propagation_matching(data, current_estimate_W_plus, current_estimate_b_plus)) ** 2)/m
        J_minus = np.sum((data - propagation_matching(data, current_estimate_W_minus,current_estimate_b_minus)) ** 2)/m

        # Update the estimate of the parameters using the minimum of the loss
        loss_cal = np.min(J_plus - J_minus)
        for i in range(L):
            # compute the estimate of the gradient
            g_t_W = loss_cal / (2.0 * deltas_W[i] * c_t)
            current_estimate_W[i] = current_estimate_W[i] - a_t * g_t_W
            g_t_b = loss_cal / (2.0 * deltas_b[i] * c_t)
            current_estimate_b[i] = current_estimate_b[i] - a_t * g_t_b
            # Ignore results that are outside the boundaries
            if (self.min_vals is not None) and (self.max_vals is not None):
                current_estimate_W[i] = np.minimum(current_estimate_W[i], self.max_vals)
                current_estimate_W[i] = np.maximum(current_estimate_W[i], self.min_vals)
                current_estimate_b[i] = np.minimum(current_estimate_b[i], self.max_vals)
                current_estimate_b[i] = np.maximum(current_estimate_b[i], self.min_vals)

        # increment the counter
        self.t += 1

        return current_estimate_W, current_estimate_b



