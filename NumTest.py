import numpy as np

from data.data_am0to9 import desired
from components.RHN import RHN
from components.Optimizer import SPSA
from components.Helper import display_image, apply_distortion


hidden_num = 20

memory = np.asarray(desired).T
'''print("***********************Original Image************************")
for i in range (len(desired)):
    display_image(desired[i], 7, 5)'''

# distort = apply_distortion(np.asarray(desired), 5)
# np.save("data\\5_pixels_distortion\\distort", distort)
distort = np.load('data\\5_pixels_distortion\\distort.npy')
test = np.asarray(distort).T

name = [
        '0_distortion', '1_distortion', '2_distortion', '3_distortion', '4_distortion',
        '5_distortion', '6_distortion', '7_distortion', '8_distortion', '9_distortion',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ]
'''print("***********************Distorted Image************************")
for i in range (len(distort)):
    display_image(distort[i], 7, 5, name[i], node_num=None)'''

test_iter = 1
for i in range(test_iter):
    # Create the optimizer & RHN
    opt_iter = 100000
    optimizer = SPSA(a=100.0, c=2.0, A=opt_iter/10.0, alpha=1.0, gamma=0.167)
    rhn = RHN(hidden_num=hidden_num, feedback_num=1, memory=memory, optimizer=optimizer, num_iterations=opt_iter)
    rhn.RHN_model()
    pred_test = rhn.predict(test).T
    print("***********************Recreated Image************************")
    for i in range (len(pred_test)):
        display_image(pred_test[i], 7, 5, name[i + 10], node_num=hidden_num)

