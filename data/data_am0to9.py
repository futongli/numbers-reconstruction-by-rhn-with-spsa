import numpy
from itertools import chain

numdata = 10
    
a0_pattern = [[0, 1, 1, 1, 0],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [0, 1, 1, 1, 0]]

a1_pattern = [[0, 0, 1, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 1, 0, 0],
              [0, 0, 1, 0, 0]]

a2_pattern = [[1, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1],
              [1, 1, 1, 1, 1],
              [1, 0, 0, 0, 0],
              [1, 0, 0, 0, 0],
              [1, 1, 1, 1, 1]]

a3_pattern = [[1, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1],
              [0, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1],
              [1, 1, 1, 1, 1]]
    
a4_pattern = [[0, 0, 0, 1, 1],
              [0, 0, 1, 0, 1],
              [0, 1, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1]]
    
a5_pattern = [[1, 1, 1, 1, 1],
              [1, 0, 0, 0, 0],
              [1, 0, 0, 0, 0],
              [1, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1],
              [1, 1, 1, 1, 1]]
    
a6_pattern = [[1, 1, 1, 1, 1],
              [1, 0, 0, 0, 0],
              [1, 0, 0, 0, 0],
              [1, 0, 0, 0, 0],
              [1, 1, 1, 1, 1],
              [1, 0, 0, 0, 1],
              [1, 1, 1, 1, 1]]
    
a7_pattern = [[1, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 1, 0],
              [0, 0, 0, 1, 0],
              [0, 0, 1, 0, 0],
              [0, 1, 0, 0, 0],
              [1, 0, 0, 0, 0]]
    
a8_pattern = [[1, 1, 1, 1, 1],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 1, 1, 1, 1],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 1, 1, 1, 1]]
    
a9_pattern = [[1, 1, 1, 1, 1],
              [1, 0, 0, 0, 1],
              [1, 0, 0, 0, 1],
              [1, 1, 1, 1, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1],
              [0, 0, 0, 0, 1]]

a0_list = list(chain.from_iterable(a0_pattern))

a1_list = list(chain.from_iterable(a1_pattern))

a2_list = list(chain.from_iterable(a2_pattern))

a3_list = list(chain.from_iterable(a3_pattern))

a4_list = list(chain.from_iterable(a4_pattern))

a5_list = list(chain.from_iterable(a5_pattern))

a6_list = list(chain.from_iterable(a6_pattern))

a7_list = list(chain.from_iterable(a7_pattern))

a8_list = list(chain.from_iterable(a8_pattern))

a9_list = list(chain.from_iterable(a9_pattern))


data  = [   
        a8_list
        ]

desired  = [   
        a0_list, a1_list, a2_list, a3_list, a4_list,a5_list, a6_list,a7_list, a8_list, a9_list,
        ]
